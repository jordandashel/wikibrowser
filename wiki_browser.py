import sys
import wikipedia

import wiki_helpers as helpers

if sys.argv[1] in ['-h', '--help']:
    helpers.print_help_message()
    sys.exit()

selection = 0

wiki_results = wikipedia.search(sys.argv[1])

if len(sys.argv) > 2 and sys.argv[2]  == '-q':
    print("What are you looking for?\n")
    selection = helpers.user_selection(wiki_results)


if len(sys.argv) > 2 and sys.argv[2] == '-c':
    page = wikipedia.page(title=wiki_results[selection])
    sections = list(filter(lambda x: len(page.section(x)) > 0, 
                        page.sections))
    selection = helpers.user_selection(sections)
    section = page.section(page.sections[selection])
    helpers.display_content(section)
    sys.exit()
        

try:
    summary = wikipedia.summary(wiki_results[selection])
    helpers.display_content("\n\t" + wiki_results[selection] + "\n\n" + summary)
except wikipedia.exceptions.DisambiguationError as e:
    print("You've reached a disambiguation page.")
    print("What are you looking for?")
    selection = helpers.user_selection(e.options)
    summary = wikipedia.summary(e.options[selection])
    helpers.display_content("\n\t" + wiki_results[selection] + "\n\n" + summary)

