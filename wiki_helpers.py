import textwrap
import pydoc

def user_selection(options):
    fmt_options = []
    for i, option in enumerate(options):
        fmt_options.append('{:3} {:30.28}'.format(str(i + 1)+'.', option))

    half = int(len(fmt_options) / 2)
    for i in range(half):
        print(fmt_options[i] + fmt_options[i+half])

    selection = int(input("\nWhich would you like to choose?: ")) - 1
    return selection

def print_help_message():
    print()
    print("WIKIPEDIA QUERIER")
    print("""
To get the summary of a wikipedia entry, run

    `wiki topic`
    
to view the top hits from a query and to select from possible
matches, use the flag `-q`, for 'query'

    `wiki topic -q`
    """)

def display_content(content):
    fmt_content = textwrap.fill(content, replace_whitespace=False)

    if len(fmt_content) > 1500:
        pydoc.pager(fmt_content)
    else:
        print(fmt_content)
    
